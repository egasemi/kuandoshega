#!/usr/bin/bash

export $(cat functions/.env | xargs)

export URL="$SERVER_URL/kuandoshega-stg/us-central1/app/webhook/$TOKEN"
export ALLOWED_UPDATES='["message","callback_query"]'

res=$(curl --write-out "%{http_code}" -d "url=$URL&drop_pending_updates=true&allowed_updates=$ALLOWED_UPDATES" -X POST https://api.telegram.org/bot$TOKEN/setWebhook -s --output /dev/null)

if [ $res == 200 ]; then  
    echo "success webhook set"
else
    echo "set webhook fail"
    echo $res
    exit 1
fi

