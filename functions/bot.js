const axios = require('axios');
const { normalizer, flagColor, errorHandler, arribosFormat, setCBData, checkSameMsj } = require('./utils');
const { getStops, getStop } = require('./ksh');
const { TOKEN } = process.env;

const TELEGRAM_API = `https://api.telegram.org/bot${TOKEN}`;

axios.defaults.baseURL = TELEGRAM_API;

const sendLocation = async (chat_id, message_id, data) => {
  let apiPath = '/sendLocation';
  if (data.id === 'update_loc') {
    apiPath = '/editMessageLiveLocation';
  } else {
    deleteMessage(chat_id, message_id);
  }

  const cuandollega = await getStop(data.stop);

  const targetBondi = cuandollega.data.find((bondi) => bondi.linea.id === data.bondi);

  const { latitud, longitud } = targetBondi.arribos[0];

  const reply_markup = {
    inline_keyboard: [
      [
        {
          text: 'Actualizar 🔄',
          callback_data: JSON.stringify({ id: 'update_loc', stop: data.stop, bondi: data.bondi }),
        },
      ],
    ],
  };

  try {
    const msj = await axios.post(apiPath, {
      chat_id,
      message_id,
      latitude: latitud,
      longitude: longitud,
      live_period: targetBondi.arribos[0].arriboEnMinutos * 60,
      reply_markup,
    });
    return msj.data.result;
  } catch (error) {
    errorHandler(error);
  }
};

const setLocationOptions = async (chat_id, message_id, data) => {
  const cuandollega = await getStop(data.stop);

  if (cuandollega.data.length === 1) {
    sendLocation(chat_id, message_id, { id: 'loc', stop: data.stop, bondi: cuandollega.data[0].linea.id });
  } else {
    const reply_markup = {
      inline_keyboard: [],
    };
    cuandollega.data.forEach((bondi) => {
      reply_markup.inline_keyboard.push([
        {
          text: `${bondi.linea.codigoEMR}${flagColor(bondi)}`,
          callback_data: JSON.stringify({
            id: 'loc',
            stop: data.stop,
            bondi: bondi.linea.id,
          }),
        },
      ]);
    });

    axios.post('/editMessageText', {
      chat_id,
      text: 'Elegí una línea',
      message_id,
      reply_markup,
    });
  }
};

const updateMessage = async (chat_id, message, data) => {
  const stopRes = await stopInfo(data.stop);
  if (checkSameMsj(stopRes.text, message.text)) {
    send(chat_id, stopRes.text, message.message_id, stopRes.reply_markup);
  }
};

const deleteMessage = async (chat_id, message_id) => {
  try {
    axios.post('/deleteMessage', { chat_id, message_id });
  } catch (error) {
    errorHandler(error);
  }
};
const send = async (chat_id, text, messageId = false, replyMarkup = false) => {
  let apiPath = '/sendMessage';

  const options = {
    chat_id,
    text,
    parse_mode: 'MarkdownV2',
    allow_sending_without_reply: true,
  };

  if (messageId) {
    apiPath = '/editMessageText';
    options['message_id'] = messageId;
  }

  if (replyMarkup) {
    options['reply_markup'] = replyMarkup;
  }

  try {
    const { data } = await axios.post(apiPath, options);
    return data.result;
  } catch (error) {
    errorHandler(error);
    return { message: 'error' };
  }
};

const answerCBQ = (cbq) => {
  axios.post('/answerCallbackQuery', { callback_query_id: cbq.id, cache_time: 10 });
};

const stopSearch = async (address) => {
  try {
    const { geometry, properties } = address;
    const name = normalizer(properties.name);

    const stops = await getStops(geometry);

    const text = `Paradas cercanas a *${name}:*\n\n`;

    const replyMarkup = {
      inline_keyboard: [],
    };

    stops.data.forEach((stop) => {
      replyMarkup.inline_keyboard.push([
        {
          text: stop.nombre,
          callback_data: setCBData('corner', stop.id),
        },
      ]);
    });

    return { text, reply_markup: replyMarkup };
  } catch (error) {
    errorHandler(error);
  }
};

const stopInfo = async (stop) => {
  try {
    const reply_markup = {
      inline_keyboard: [[{ text: 'Actualizar 🔄', callback_data: setCBData('act', stop) }]],
    };

    const cuandollega = await getStop(stop);

    const bondis = cuandollega.data;

    if (bondis.length > 0) {
      reply_markup.inline_keyboard.push([{ text: 'Ver en el mapa 📌', callback_data: setCBData('map_options', stop) }]);
      bondis.sort((a, b) => {
        if (a.arribos[0].arriboEnMinutos > b.arribos[0].arriboEnMinutos) {
          return 1;
        }
        if (a.arribos[0].arriboEnMinutos < b.arribos[0].arriboEnMinutos) {
          return -1;
        }

        return 0;
      });

      var text = `Próximos arribos de la parada */${stop}*\n\n`;

      bondis.forEach((bondi) => {
        text +=
          `*${bondi.linea.codigoEMR}* ` +
          `${flagColor(bondi)} : ` +
          `${arribosFormat(bondi.arribos)}` +
          `${bondi.arribos[1] ? ', ' + bondi.arribos[1].arriboEnMinutos + ' min' : ''}\n`;
      });
      return { text, reply_markup };
    } else {
      return {
        text: normalizer(`No hay próximos arribos a la parada */${stop}* 😔`),
        reply_markup,
      };
    }
  } catch (error) {
    errorHandler(error);
    return { text: 'La parada no existe' };
  }
};

module.exports = {
  send,
  stopSearch,
  errorHandler,
  stopInfo,
  answerCBQ,
  sendLocation,
  deleteMessage,
  setLocationOptions,
  updateMessage,
};
