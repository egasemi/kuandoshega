const { onRequest } = require('firebase-functions/v2/https');

require('dotenv').config();
const express = require('express');
const { normalizer, errorHandler } = require('./utils.js');
const { send, stopSearch, stopInfo, answerCBQ, setLocationOptions, sendLocation, updateMessage } = require('./bot.js');
const { getLocation, getAddress } = require('./ksh.js');

const { TOKEN } = process.env;

const URI = `/webhook/${TOKEN}`;

const app = express();

app.use(express.json());

app.post(URI, async (req, res) => {
  if (req.body.callback_query) {
    const cbq = req.body.callback_query;

    const cbqData = JSON.parse(cbq.data);

    try {
      if (cbqData.id === 'map_options') {
        setLocationOptions(cbq.message.chat.id, cbq.message.message_id, cbqData);
      } else if (['loc', 'update_loc'].includes(cbqData.id)) {
        sendLocation(cbq.message.chat.id, cbq.message.message_id, cbqData);
      } else if (['corner', 'act'].includes(cbqData.id)) {
        updateMessage(cbq.message.chat.id, cbq.message, cbqData);
      }
      answerCBQ(cbq);
    } catch (error) {
      errorHandler(error);
      return res.end();
    }

    return res.end();
  }
  const chat_id = req.body.message?.chat.id;
  const reqText = req.body.message?.text?.replace('/', '');
  var loadingMsj = {};

  if (reqText === 'start') {
    var reply_markup = {
      keyboard: [[{ text: 'Enviar Ubicación', callback_data: 'test', request_location: true }]],
      resize_keyboard: true,
      is_persistent: false,
    };
    send(
      chat_id,
      'Holi, mandame el número de parada y te respondo con los próximos arribos\\.\n\nTambién podés mandarme una dirección, una esquina o tu ubicación y te respondo con las paradas cercanas',
      false,
      reply_markup
    );
    res.end();
  } else {
    loadingMsj = await send(chat_id, normalizer('Cargando...'));

    if (!isNaN(parseInt(reqText))) {
      console.log('busqueda parada ', reqText);
      const stopRes = await stopInfo(reqText);

      send(chat_id, stopRes.text, loadingMsj.message_id, stopRes.reply_markup);
      return res.end();
    } else if (req.body.message.location) {
      const { latitude, longitude } = req.body.message.location;
      //console.log('busqueda ubicación ', `lat: ${latitude}, long: ${longitude}`);

      const dir = await getLocation(latitude, longitude);

      const stops = await stopSearch(dir.data);

      send(chat_id, stops.text, loadingMsj.message_id, stops.reply_markup);

      return res.end();
    } else if (/^(?!start\b)[\w\s]+$/.test(normalizer(reqText))) {
      console.log('busqueda dirección ', reqText);
      const esquinas = await getAddress(reqText);

      var stops = {
        text: 'La dirección o esquina no existe ¬¬',
        message_id: loadingMsj.message_id,
      };

      if (esquinas.data.features.length !== 0 && esquinas?.data.features[0].geometry !== null) {
        stops = await stopSearch(esquinas?.data.features[0]);
      }

      send(chat_id, stops.text, loadingMsj.message_id, stops.reply_markup);
    } else {
      send(chat_id, '🤌', loadingMsj.message_id);
    }
  }

  return res.end();
});

exports.app = onRequest(app);
