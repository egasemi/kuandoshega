#!/usr/bin/bash

export $(cat functions/.env | xargs)

res=$(curl --silent https://api.telegram.org/bot$TOKEN/getWebhookInfo)

echo $res

